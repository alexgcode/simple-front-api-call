/*
fetch("https://quotes15.p.rapidapi.com/quotes/random/", {
	"method": "GET",
	"headers": {
		"x-rapidapi-key": "86e83437aamsh3ded97dc08751bcp168435jsnd74c9b69b1d2",
		"x-rapidapi-host": "quotes15.p.rapidapi.com"
	}
})
.then(response => response.json())
.then(response => {
	console.log(response);
})
.catch(err => {
	console.error(err);
});
*/

let showButton = document.querySelector("#displayNasa");

showButton.addEventListener("click", () => {
	sendApiRequest();
});

async function sendApiRequest(){
	let API_KEY = "";
	let response = await fetch('https://api.nasa.gov/planetary/apod?api_key='+API_KEY);
	
	let data = await response.json();
	console.log(data);
	useApiData(data);
}

function useApiData(data){
	document.querySelector("#content").innerHTML += data.explanation;
	document.querySelector("#content").innerHTML += "<img src="+data.url+">";
}
